/**
  ******************************************************************************
  * @file ultimate_hacking_keyboard_v2/Src/bsp.c
  * @author  Rafael Lee
  * @version V0.0.1
  * @date
  * @brief   bsp.c module
  ******************************************************************************
  */


/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f4xx_hal.h"
#include "bsp.h"
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/

// the sequence should be the same with enum GPIO_LIST defined in bsp.h
gpio_state_struct g_gpio_state_list[] = \
{
  {KEY_ROW_02_Pin, KEY_ROW_02_GPIO_Port, GPIO_PIN_RESET},
  {KEY_ROW_05_Pin, KEY_ROW_05_GPIO_Port, GPIO_PIN_RESET},
  {KEY_ROW_04_Pin, KEY_ROW_04_GPIO_Port, GPIO_PIN_RESET},
  {KEY_ROW_03_Pin, KEY_ROW_03_GPIO_Port, GPIO_PIN_RESET},
  {SPI2_MOSI_LED_Pin, SPI2_MOSI_LED_GPIO_Port, GPIO_PIN_RESET},
  {KEY_ROW_01_Pin, KEY_ROW_01_GPIO_Port, GPIO_PIN_RESET},
  {KEY_ROW_00_Pin, KEY_ROW_00_GPIO_Port, GPIO_PIN_RESET},
  {EEPROM_nCS_Pin, EEPROM_nCS_GPIO_Port, GPIO_PIN_RESET},
  {EEPROM_nHOLD_Pin, EEPROM_nHOLD_GPIO_Port, GPIO_PIN_RESET},
  {EEPROM_nWP_Pin, EEPROM_nWP_GPIO_Port, GPIO_PIN_RESET},
  {SPI1_SCK_KEY_Pin, SPI1_SCK_KEY_GPIO_Port, GPIO_PIN_RESET},
  {SPI1_SS_GPIO_KEY_Pin, SPI1_SS_GPIO_KEY_GPIO_Port, GPIO_PIN_SET}, // SET
  {SPI1_MOSI_KEY_Pin, SPI1_MOSI_KEY_GPIO_Port, GPIO_PIN_RESET},
  {LED_ROW_DRIVE_05_Pin, LED_ROW_DRIVE_05_GPIO_Port, GPIO_PIN_RESET},
  {LED_ROW_DRIVE_04_Pin, LED_ROW_DRIVE_04_GPIO_Port, GPIO_PIN_RESET},
  {SPI2_SS_GPIO_LED_Pin, SPI2_SS_GPIO_LED_GPIO_Port, GPIO_PIN_SET}, // SET
  {BOOT1_Pin, BOOT1_GPIO_Port, GPIO_PIN_RESET},
  {SPI2_SCK_LED_Pin, SPI2_SCK_LED_GPIO_Port, GPIO_PIN_RESET},
  {GPIO_CAPS_LOCK_LED3_Pin, GPIO_CAPS_LOCK_LED3_GPIO_Port, GPIO_PIN_RESET}, // confirmed
  {GPIO_NUM_LOCK_LED2_Pin, GPIO_NUM_LOCK_LED2_GPIO_Port, GPIO_PIN_RESET}, // confirmed
  {GPIO_SCROLL_LOCK_LED1_Pin, GPIO_SCROLL_LOCK_LED1_GPIO_Port, GPIO_PIN_RESET},
  {LED_ROW_DRIVE_02_Pin, LED_ROW_DRIVE_02_GPIO_Port, GPIO_PIN_RESET},
  {LED_ROW_DRIVE_01_Pin, LED_ROW_DRIVE_01_GPIO_Port, GPIO_PIN_RESET},
  {LED_ROW_DRIVE_00_Pin, LED_ROW_DRIVE_00_GPIO_Port, GPIO_PIN_RESET},
  {KEY_ROW_06_Pin, KEY_ROW_06_GPIO_Port, GPIO_PIN_RESET},
  {SPI3_SCK_EEPROM_Pin, SPI3_SCK_EEPROM_GPIO_Port, GPIO_PIN_RESET},
  {SPI3_MISO_EEPROM_Pin, SPI3_MISO_EEPROM_GPIO_Port, GPIO_PIN_RESET},
  {SPI3_MOSI_EEPROM_Pin, SPI3_MOSI_EEPROM_GPIO_Port, GPIO_PIN_RESET},
  {LED_ROW_DRIVE_03_Pin, LED_ROW_DRIVE_03_GPIO_Port, GPIO_PIN_RESET},
  {I2C1_SCL_OLED_Pin, I2C1_SCL_OLED_GPIO_Port, GPIO_PIN_RESET},
  {I2C1_SDA_OLED_Pin, I2C1_SDA_OLED_GPIO_Port, GPIO_PIN_RESET},
  {TIM4_CH3_LED4_Pin, TIM4_CH3_LED4_GPIO_Port, GPIO_PIN_RESET}
};

/* Private function prototypes -----------------------------------------------*/

/* Private functions ---------------------------------------------------------*/


/**
 * \par Function
 *   void GPIO_activate (GPIO_LIST x)
 * \par Description
 *   Activate the pin using the info defined in the array
 * \par Output
 *   None
 * \par return
 *   None
 * \par Others
 *   None
 */
void GPIO_activate (GPIO_LIST x)
{
  uint32_t y = (uint32_t)x;
  HAL_GPIO_WritePin (g_gpio_state_list[y].gpio_port, g_gpio_state_list[y].gpio_pin, g_gpio_state_list[y].gpio_state);
}

/**
 * \par Function
 *   GPIO_deactivate (GPIO_LIST x)
 * \par Description
 *   Deactivate the pin using the info defined in the array
 * \par Output
 *   None
 * \par return
 *   None
 * \par Others
 *   None
 */
void GPIO_deactivate (GPIO_LIST x)
{
  uint32_t y = (uint32_t)x;
  HAL_GPIO_WritePin (g_gpio_state_list[y].gpio_port, g_gpio_state_list[y].gpio_pin, (GPIO_PIN_SET == g_gpio_state_list[y].gpio_state ? GPIO_PIN_RESET : GPIO_PIN_SET));
}

/**
 * \par Function
 *   GPIO_toggle (GPIO_LIST x)
 * \par Description
 *   Toggle the pin using the info defined in the array
 * \par Output
 *   None
 * \par return
 *   None
 * \par Others
 *   None
 */
void GPIO_toggle (GPIO_LIST x)
{
  uint32_t y = (uint32_t)x;
  HAL_GPIO_TogglePin (g_gpio_state_list[y].gpio_port, g_gpio_state_list[y].gpio_pin);
}


/* ---------------------------------------------------------------------------*/
/**
  * @brief
  * @param  None
  * @retval None
  */
/* ---------------------------------------------------------------------------*/

/************************ (C) COPYRIGHT ************************END OF FILE****/
