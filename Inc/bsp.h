#ifndef __BSP_H
#define __BSP_H
/**
******************************************************************************
* @file
* @author  Rafael Lee
* @version V0.0.1
* @date
* @brief   Header for bsp.c
******************************************************************************
*/

/* Define to prevent recursive inclusion -------------------------------------*/

/* Includes ------------------------------------------------------------------*/
/* Exported types ------------------------------------------------------------*/
// the sequence should be the same with g_gpio_state_list[][3] defined in bsp.c
typedef enum
{
  KEY_ROW_02 = 0,
  KEY_ROW_05,
  KEY_ROW_04,
  KEY_ROW_03,
  SPI2_MOSI_LED,
  KEY_ROW_01,
  KEY_ROW_00,
  EEPROM_nCS,
  EEPROM_nHOLD,
  EEPROM_nWP,
  SPI1_SCK_KEY,
  SPI1_SS_GPIO_KEY,
  SPI1_MOSI_KEY,
  LED_ROW_DRIVE_05,
  LED_ROW_DRIVE_04,
  SPI2_SS_GPIO_LED,
  BOOT1,
  SPI2_SCK_LED,
  GPIO_CAPS_LOCK_LED3,
  GPIO_NUM_LOCK_LED2,
  GPIO_SCROLL_LOCK_LED1,
  LED_ROW_DRIVE_02,
  LED_ROW_DRIVE_01,
  LED_ROW_DRIVE_00,
  KEY_ROW_06,
  SPI3_SCK_EEPROM,
  SPI3_MISO_EEPROM,
  SPI3_MOSI_EEPROM,
  LED_ROW_DRIVE_03,
  I2C1_SCL_OLED,
  I2C1_SDA_OLED,
  TIM4_CH3_LED4,
} GPIO_LIST;

typedef struct
{
  uint16_t gpio_pin;
  GPIO_TypeDef *gpio_port;
  GPIO_PinState gpio_state;
} gpio_state_struct;


/* Exported constants --------------------------------------------------------*/


/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */

void GPIO_activate (GPIO_LIST x);

void GPIO_deactivate (GPIO_LIST x);

void GPIO_toggle (GPIO_LIST x);



/************************ (C) COPYRIGHT ************************END OF FILE****/

#endif /* __BSP_H */
