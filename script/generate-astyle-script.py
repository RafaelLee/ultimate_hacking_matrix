#!/usr/bin/python
# coding:utf-8
# Filename: 'generate-astyle-script.py'
# cmd /k C:\Python33\python.exe "$(FULL_CURRENT_PATH)" & ECHO. & PAUSE & EXIT
# cmd /k C:\Python27\python.exe "$(FULL_CURRENT_PATH)" & ECHO. & PAUSE & EXIT
from __future__ import print_function
import sys,os,time,re,urllib,copy
# sys.path.append(r'C:\programs\Python27\Lib\site-packages\chardet-1.1-py2.7.egg')
# import chardet

os.chdir(str(os.path.split(os.path.realpath(__file__))[0]))
print('os.getcwd()=%s'%os.getcwd())
print(time.strftime("%Y-%m-%d %H:%M:%S",time.localtime()))
time.clock()
print('%0.3f seconds'%time.clock())

#------------------------------------------------------------------------------
import math
import copy
import io
dirname='..'

ftemp = io.StringIO()

# astyle_script = u'astyle.sh "'
astyle_script = u'astyle -A1 -s2 -H -U -Y -p -xg -xd -xb -H -y -j -J -c -S -z2 -k3 -N --suffix=.bak "'
for root, dirs, files in os.walk(dirname):
    for subdir in sorted(dirs):
        # print(os.path.join(root, subdir), end='-----------------\n')
        # print('-----------------')
        pass
    for filename in sorted(files):
        # print(filename)
        fullpath = os.path.join(root,filename)
        # http://stackoverflow.com/questions/3167154/how-to-split-a-dos-path-into-its-components-in-python
        folders = []
        path=copy.deepcopy(fullpath)
        while 1:
            path, folder = os.path.split(path)
            if folder != "":
                folders.append(folder)
            else:
                if path != "":
                    folders.append(path)
                break
        folders.reverse()

        # if folders[1] not in ('Drivers', 'Src/communication/nano_pb' 'Inc/communication/nano_pb'):
        # print('filename = %s' %filename)
        # print('folders = %s' %folders)
        # print('/'.join(folders[:2]))
        # print(('/'.join(folders[:4])))
        # if '/'.join(folders) not in ('../Drivers', 'nano_pb' 'nano_pb'):
        if ('/'.join(folders[:2]) not in set(['../Drivers'])) \
           and ('/'.join(folders[:4]) not in set(['../Src/communication/nano_pb', '../Inc/communication/nano_pb'])):
            if os.path.splitext(filename)[-1] in ('.c','.h','.cc','.hh'):
                print(fullpath)
                # ftemp.write(u'./dr-indent-cpp.sh \"')
                ftemp.write(astyle_script)
                ftemp.write(fullpath.decode('utf-8')) #.encode('utf-8'))
                ftemp.write(u'\"\n')


########## dr-indent-cpp.sh
# #!/bin/bash
# astyle  --lineend=linux --pad-oper --align-pointer=type --style=allman --align-reference=type --convert-tabs --keep-one-line-blocks --close-templates --add-brackets --pad-header --indent=spaces=2 $1

with open('astyle.sh', 'wb') as fw:
    fw.write("""#!/bin/sh\n
RUNNING_PATH=${PWD}
RELATED_PATH=${0%/*}
cd ${RUNNING_PATH}
cd ${RELATED_PATH}

""")
    fw.write(ftemp.getvalue().encode('utf-8'))

# ftemp.close()
