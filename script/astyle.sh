#!/bin/sh

RUNNING_PATH=${PWD}
RELATED_PATH=${0%/*}
cd ${RUNNING_PATH}
cd ${RELATED_PATH}

astyle -A1 -s2 -H -U -Y -p -xg -xd -xb -H -y -j -J -c -S -z2 -k3 -N --suffix=.bak "../Src/bsp.c"
astyle -A1 -s2 -H -U -Y -p -xg -xd -xb -H -y -j -J -c -S -z2 -k3 -N --suffix=.bak "../Src/main.c"
astyle -A1 -s2 -H -U -Y -p -xg -xd -xb -H -y -j -J -c -S -z2 -k3 -N --suffix=.bak "../Src/stm32f4xx_hal_msp.c"
astyle -A1 -s2 -H -U -Y -p -xg -xd -xb -H -y -j -J -c -S -z2 -k3 -N --suffix=.bak "../Src/stm32f4xx_it.c"
astyle -A1 -s2 -H -U -Y -p -xg -xd -xb -H -y -j -J -c -S -z2 -k3 -N --suffix=.bak "../Src/syscalls.c"
astyle -A1 -s2 -H -U -Y -p -xg -xd -xb -H -y -j -J -c -S -z2 -k3 -N --suffix=.bak "../Src/system_stm32f4xx.c"
astyle -A1 -s2 -H -U -Y -p -xg -xd -xb -H -y -j -J -c -S -z2 -k3 -N --suffix=.bak "../Src/usb_device.c"
astyle -A1 -s2 -H -U -Y -p -xg -xd -xb -H -y -j -J -c -S -z2 -k3 -N --suffix=.bak "../Src/usbd_conf.c"
astyle -A1 -s2 -H -U -Y -p -xg -xd -xb -H -y -j -J -c -S -z2 -k3 -N --suffix=.bak "../Src/usbd_desc.c"
astyle -A1 -s2 -H -U -Y -p -xg -xd -xb -H -y -j -J -c -S -z2 -k3 -N --suffix=.bak "../Inc/bsp.h"
astyle -A1 -s2 -H -U -Y -p -xg -xd -xb -H -y -j -J -c -S -z2 -k3 -N --suffix=.bak "../Inc/main.h"
astyle -A1 -s2 -H -U -Y -p -xg -xd -xb -H -y -j -J -c -S -z2 -k3 -N --suffix=.bak "../Inc/stm32f4xx_hal_conf.h"
astyle -A1 -s2 -H -U -Y -p -xg -xd -xb -H -y -j -J -c -S -z2 -k3 -N --suffix=.bak "../Inc/stm32f4xx_it.h"
astyle -A1 -s2 -H -U -Y -p -xg -xd -xb -H -y -j -J -c -S -z2 -k3 -N --suffix=.bak "../Inc/usb_device.h"
astyle -A1 -s2 -H -U -Y -p -xg -xd -xb -H -y -j -J -c -S -z2 -k3 -N --suffix=.bak "../Inc/usbd_conf.h"
astyle -A1 -s2 -H -U -Y -p -xg -xd -xb -H -y -j -J -c -S -z2 -k3 -N --suffix=.bak "../Inc/usbd_desc.h"
astyle -A1 -s2 -H -U -Y -p -xg -xd -xb -H -y -j -J -c -S -z2 -k3 -N --suffix=.bak "../Middlewares/ST/STM32_USB_Device_Library/Core/Inc/usbd_core.h"
astyle -A1 -s2 -H -U -Y -p -xg -xd -xb -H -y -j -J -c -S -z2 -k3 -N --suffix=.bak "../Middlewares/ST/STM32_USB_Device_Library/Core/Inc/usbd_ctlreq.h"
# astyle -A1 -s2 -H -U -Y -p -xg -xd -xb -H -y -j -J -c -S -z2 -k3 -N --suffix=.bak "../Middlewares/ST/STM32_USB_Device_Library/Core/Inc/usbd_def.h"
astyle -A1 -s2 -H -U -Y -p -xg -xd -xb -H -y -j -J -c -S -z2 -k3 -N --suffix=.bak "../Middlewares/ST/STM32_USB_Device_Library/Core/Inc/usbd_ioreq.h"
astyle -A1 -s2 -H -U -Y -p -xg -xd -xb -H -y -j -J -c -S -z2 -k3 -N --suffix=.bak "../Middlewares/ST/STM32_USB_Device_Library/Core/Src/usbd_core.c"
astyle -A1 -s2 -H -U -Y -p -xg -xd -xb -H -y -j -J -c -S -z2 -k3 -N --suffix=.bak "../Middlewares/ST/STM32_USB_Device_Library/Core/Src/usbd_ctlreq.c"
astyle -A1 -s2 -H -U -Y -p -xg -xd -xb -H -y -j -J -c -S -z2 -k3 -N --suffix=.bak "../Middlewares/ST/STM32_USB_Device_Library/Core/Src/usbd_ioreq.c"
astyle -A1 -s2 -H -U -Y -p -xg -xd -xb -H -y -j -J -c -S -z2 -k3 -N --suffix=.bak "../Middlewares/ST/STM32_USB_Device_Library/Class/HID/Inc/usbd_hid.h"
astyle -A1 -s2 -H -U -Y -p -xg -xd -xb -H -y -j -J -c -S -z2 -k3 -N --suffix=.bak "../Middlewares/ST/STM32_USB_Device_Library/Class/HID/Src/usbd_hid.c"
